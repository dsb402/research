
Project Review
=====================================================

## Overview

1) Briefly summarize the experiment in this project.
This project compares the performance of DCTCP and TCP cubic with regards to the Queue Length, Queue Delay,
as stated by the author.
The project observes the throughput for long lived background flows and short lived queries for TCP cubic and DCTCP,
this has been clearly shown in the two graphs that were uploaded by the author, one for the increasing number of
flows(Queue Length) and the other with varying delays (Query delay).
The project studies the fraction of queries that time out when multiple servers are used, as stated by the author,
there is no clear explanation regarding the same. Also the goal stated by the author in the “Report” document doesn’t
talk about this Metric but the “Proposal” document does mention it.



2) Does the project generally follow the guidelines and parameters we have
learned in class?
The project does follow the guidelines and parameters learned in class:
a) The Metrics throughput is legitimate.
This metric help us observe the difference in the performance of TCP cubic and DCTCP.
The variating has been done by changing the number of flows while using iperf and by introducing different values of delay.
b) The parameter that is being varied is the Number of servers, as the author states in the proposal. Although, from my observation
it seems to me that the parameters are Delay and No.of Flows.
c) The graphs that have been created using Rscript show the result that the experiment is trying to convey.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
The goal of the experiment reads as follows, "To calibrate throughput for long lived background flows and latency
for latency-sensitive Short query flows by using DCTCP over conventional TCP in Data center environment".
It is a focused, specific goal. It specifies which two categories are being compared, i.e. TCP and DCTCP and also the basis
on which they are being compared,i.e. the Short lived flows and Long lived flows.
The graphs show useful and interesting results, since we can clearly see in the graphs that the throughput of TCP saturates and
shows a constant output whereas DCTCP has a increasing throughput which is a desirable result for real world usage in the Data
Center environment.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
The metrics chosen are appropriate.
Throughput which is one of the metrics shows exactly how the variations of TCP and DCTCP take place
on inserting delay and increasing the number of flows, which in turn reflect on the Goal of this project, i.e. “"To calibrate throughput
for long lived background flows and latency  for latency-sensitive Short query flows by using DCTCP over conventional TCP in
Data center environment"
Although the parameter, i.e. No.of servers is not appropriate. The report states that the servers in are rack are Client an Client1, but
the output of the experiment show the interactions between client and server and no interactions between client1 and server, I am unsure if
no.of servers is the correct parameter.
Having said that using one server and client does server the purpose of comparing TCP and DCTCP in Short lived and Long lived flows.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
Yes, it takes 3 different flow lengths and 3 different delays each for TCP and DCTCP which is in total 12 sets of iperf readings.
The graphs made using these 12 sets of readings clearly shows the desired output.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
The metric throughput is the correct metric for comparing DCTCP and TCP, since theoretically
the basis of difference between the two is shown by the throughput variations on increasing the number of flows and and on testing
the behavior incase of flows with a large delay.
The basis of a new TCP for data centers was specifically for flows with a large delay and many short flows, these are the places where
the traditional TCP fails to perform well.
Having said that Query delay and No.of flows should be considered as the parameters.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
According to me, it would be safe to say that the parameters to be considered should have been the number of flows and the delay,
which the author states are the metrics being measured.
The parameters in the proposal for the project read as “No.of servers”.
Although the Report does specify the following step “simultaneously we started one more IPERF UDP listening mode in server.
From the other client (client2), we start a small UDP IPERF for 1 second or two to resemble short query flows. As we know that IPERF UDP test will
yield network delay jitter”, but the steps in the “Readme” file do not give instructions for the same.


6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
Yes, the interactions between client and server (see the topology.png of 023) sufficiently address the goal of the author.
Although, a set of interactions between client2 and server would give a more definitive result and a clearer proof which would state that
the behavior of DCTCP always gives a better throughput value than TCP on considering the above parameters.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
Yes, it is almost realistic.
I use the word almost since it is difficult to show a real like data center environment by only using a few resources.


## Communicating results


1) Do the authors report the quantitative results of their experiment?
Yes, the results of all the throughput outputs have been documented and the graph using Rstudio is presumably
made using those results.

2) Is there information given about the variation and/or distribution of
experimental results?
In the “report” file the author specifies exactly what parameters need to be varied and why the same is being done.


3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
The graphs do match the desired output, to me it does seem legitimate.
Although, I haven’t been able to perform the experiment myself since the Ubuntu 14.0 was not getting installed on my
machine without which I was unable to install the DCTCP package.


5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
Yes, the expected conclusion about the Throughput behavior of TCP and DCTCP are supported by the two graphs shown
by the author.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
The authors did not include an Rspec.

The authors did not provide the ssh keys for an already reserved set of resources.

The authors did provide sufficient instructions to install DCTCP kernel for Ubuntu 14.0.
I was able to upgrade my machine to the required version by using “sudo do-release-upgrade”, but installing the required
dctcp kernel was unsuccessful.
Methods tried - The commands given in the “Readme” instructions and “https://github.com/myasuda/DCTCP-Linux”.

The authors provided a topology diagram, from which I could understand the topology that I had to design.
I reserved 4 VM's which acted as the following :
a)Router
b)Client 1
c)Client 2
d)Server
I am not aware of any internal configurations(if there were any, e.g. tunneling or Routable IP) since I did not have an Rspec
or any specific instructions regarding the same.
Instructions for installing the kernel for DCTCP and xorp on the router were given precisely.
Although while installing the kernel for DCTCP I got the error " No Volume groups found", because of this I was unable
to run the experiment for DCTCP.
After which I upgraded my VM to Ubuntu 14.0, even after this  was unable to install the kernel 3.18.3 for DCTCP and the
same error continued to occur, and the “name -r” command showed that the kernel was not successfully activated.


2) Were you able to successfully produce experiment results?
No, I wasn’t able to reproduce the experiment since the required kernel did not get installed successfully.

3) How long did it take you to run this experiment, from start to finish?
It took roughly a day and a half to run the experiment with all the necessary trial and error.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
No major changes were needed to be made.
I did try a variation to install the 3.18.3 kernel, “https://github.com/myasuda/DCTCP-Linux”, but this not not work either.
Two commands had to be changed, which I am presuming were typos in the “Readme” document:
Original commands :
sysctl -w ipv4.net.tcp__congestion_control=cubic
sysctl net.ipv4.tcp__congestion_control

Modified commands :
sysctl -w net.ipv4.tcp_congestion_control=cubic
sysctl net.ipv4.tcp_congestion_control

These extra steps took about half a day, only because of the kernel.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
This would be a degree 3 according to me, only because the kernel and ubuntu version seemed a difficult to get right, and the resources took
very long to get reserved.
Moreover “xorp” commands in itself take about 45 minutes.


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
