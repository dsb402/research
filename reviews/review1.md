
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

  Since data centers have a cluster of servers which provide services to different applications, the volume of demand received and processed by data center is very large that conventional TCP will give away bandwidth, query flows suffer delay which is undesirable. When multiple servers send out data, the out queue of TOP of the Rack (TOR) Switch gets flooded and drops/delays the packet.
  To prevent this, DCTCP was introduced which uses Explicit Congestion Notification method along with modern commodity switch router which has ECN-enabled to avoid the congestion, regulate throughput and keep the queue length limited.
  Thus consequently, long flows will get good throughput, short flows would not be delayed and also it could accommodate burst traffic.

   Thus in this experiment to support the above lines Throughput,Queue length, Query delay and Fraction of queries that suffered timeout are the performances need to be measured for dctcp in case of multiple servers with reference to traditional tcp.


2) Does the project generally follow the guidelines and parameters we have
learned in class?

 The guidelines in the project and parameters which are to be varied in this project are bounded within the lecture learnt in the class


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

The goal of the experiment is to evaluate the performance of DCTCP by calibrating the throughput for long lived background flows and latency for latency-sensitive Short query flows by using DCTCP over conventional TCP in Data center environment.
Yes, the main goal is to prove the performance of dctcp in data centre environment is much way better than the traditional tcp.
The results are said to be useful as the differnce in the throughput, latency, delay time between the dctcp and a regular tcp is clearly seen.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

The metrics to be measured in this experiment are Throughput, Query delay and Fraction of queries that suffered timeout
The parameters to be varied are the number of servers (1 to 25)
Th metrics and parameters chosen in the experiment support the experiment goal but then :
varying them with the given resources such as topology is not appropriate.

The experiment design is incompatible since dctcp enabled kernel compiled in the vm cannot activate the dctcp.( The topology is not compatible )
The experimenters done a good job in figuring out to enable the dctcp but ended up with errors
They finally took the simple topology with two virtual machines  of m1.medium node type and ubunto 14.04 disk image  and tried to reserve resourses which also ended ip with error.


3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

Yes, considering only two factors such as throughput and latency. Maximum information can be obtained by varying them between the dctcp and tcp with minimum number of trials.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

The metrics selected for study of performance of dctcp in data centre environments is right and clear as they lead a correct conclusions in evaluating the performance of dctcp in refernce to tcp.
The metrics taken in the experiment are best suited for this experiment.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

The parameters taken in the experiment for varying are the number of servers  ( from 1 to 25 ) which is out of scope of this project about the ranges so to resemble the data centre the experimeters had setup  two clients which represents servers in rack, one router which resemble TOR switch and one server which supersedes for aggregator but faced the topology issue in reserving resources.

Software setup:
6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

Since enabling dctcp is not achieved, the authors could not address

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

The comparisions are reasonable in providing appropriate and realistic baseline diffrences between the entities measured.

## Communicating results


1) Do the authors report the quantitative results of their experiment?

The authors reported the quantitative results of their experiment by taking a simple topology of one client and one server.

2) Is there information given about the variation and/or distribution of
experimental results?

Yes, the information about variation in the experimental results is shown by taking another topology and the final output results and graphs are available.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

The authors mentioned in the report that by failing in reserving the resources due to incompatibility in implementing the actual topology, they opted a simple topology with a client and server of medium.m1 node type and ubunto 14.04 disc image which too failed to reserve the resources.Finally they sort to update the kernel and could get the results
They finally able to show the output variations


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

The data is presented in a clear way and the graphs shown clearly resemble the output variations

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

Yes, the conclusions are supported by the results shown by them

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

The instructions are clear as they produced all the data in reproducing the experiment

2) Were you able to successfully produce experiment results?

I couldn't able to reproduce the whole experiment due to the topolgy issues which caused the authors also in failing to produce using the given sources
3) How long did it take you to run this experiment, from start to finish?

7 days

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

  Yes in finding the topology so that the dctcp could be enabled

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

The results cannot seem to be reproduced
by an independent researcher

## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
