
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.
```
The authors initially aimed to achieve a lot from this project but unfortunately were unable to achieve it.

**What the authors aimed at achieving as per the project report**
1.	Initially, server is made to listen to IPERF at its default port 5001.
2.	We sweep the value of K at router from 2-10 with a gap of 2 and see the variation of throughput of long background flows by starting IPERF session from a client with N>4 flows.
3.	We have to make the link between router and server which has capacity of 500Mbps as bottleneck by increasing the window size option in IPERF. We start the IPERF session from one
	client for indefinite period to resemble long flows.
4.	As we are using IPERF, we will get throughput of the flows with variation of K.
5.	Simultaneously we started one more IPERF UDP listening mode in server.
6.	From the other client, we start a small UDP IPERF for 1 second or two to resemble short query flows. As we know that IPERF UDP test will yield network delay jitter.
	This will give us the latency of all short flows for different K (Marking threshold).

**Experiment actually conducted by the authors in this project**
1.	XORP is installed
2.	Ecn is enabled on the router
3.	Updated kernel to 3.18.3 in Ubuntu 14.04 64 bit (DCTCP enabled kernel) only on Server & Client
4.	Varied delay (50ms, 100ms, 200ms) for Cubic and collected client iperf
5.	Varied delay (50ms, 100ms, 200ms) for DCTCP and collected client iperf
6.	Varied number of flows (4, 8, 12) for Cubic and collected client iperf
7.	Varied number of flows (4, 8, 12) for DCTCP and collected client iperf
8.	Plot data using Rstudio

```


2) Does the project generally follow the guidelines and parameters we have
learned in class?
```
In general, the project follows the guidelines we have learned in class. They have created reproducible data to a major extent.
They have even taken minimum number of trials necessary to obtain necessary data. It is excellent that they have mentioned the difficulties they faced while creating the project.
There are a few areas which could use improvement. The authors have not provided a conclusion to their data collected.
In the experiment design it would be useful if they mentioned the range of the parameters varied.
```



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
```
GOAL: “To calibrate throughput for long lived background flows and latency for latency-sensitive Short query flows by using DCTCP over conventional TCP in Data center environment.”

This goal seems to be perplexing and a little vague. They are trying to “Calibrate” throughput and latency. They have not mentioned in the goal why they are trying to calibrate the throughput and latency.
Let us assume that they are trying to “Measure” the throughput and latency to analyze the performance of DCTCP and Cubic. In that case the goal makes sense. The authors could have been more specific in terms of the range.
```

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
```
The goal and the experiment conducted are not related to each other.

As per the goal:
Throughput should be calibrated for long lived background flows
Latency should be calibrated for latency-sensitive short query flows
These measurements should be conducted over DCTCP and TCP cubic

In the actual experiment conducted, the authors have measured the throughput while varying delay and number of flows over DCTCP and TCP Cubic.

Metrics		: 	Throughput
Parameters	: 	TCP variant (DCTCP, Cubic)
				Delay (50ms, 100ms, 200ms)
				Number of flows (4, 8, 12)
```

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
```
As per the experiment finally conducted by the authors, they have obtained maximum information with the minimum number of trials.
```

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
```
The experiment goal is ambiguous. So I cannot determine any extra metrics that should be taken into consideration that are suited for the goal.
However as per the experiment conducted, the metric measured is throughput. Throughput is a good metric to measure DCTCP and TCP cubic.
Assuming that they are trying to compare Cubic and DCTCP, then they could also measure Packet drop rate, and resilience.
```

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
```
Yes, the parameters varied are good. They have varied Delay, TCP variant and Number of flows
```

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
```
Yes, they have taken into account the possible interactions between parameters. First they have compared throughput
with number of flows and TCP variants. Second they have measured Throughput with Delay and TCP variants.
```


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
```
Yes, the baseline selected for comparison is appropriate for this project and our scope. But realistically there are better comparisons
that should be made to understand the performance for DCTCP in a real data center environment.
```


## Communicating results


1) Do the authors report the quantitative results of their experiment?
```
No, the authors do not provide an analysis of the data they have collected.
```

2) Is there information given about the variation and/or distribution of
experimental results?
```
No, the authors do not provide an analysis of the data they have collected.
```

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
```
The graphs plotted are accurate as per the data collected by them. Unfortunately the authors do not provide a written
analysis of the data they have collected. So their graphs speak the truth based on the data collected.
But the authors have not written down any conclusion to make their results either good or bad.
```

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
```
With the data collected by the authors, the graphs are plotted well. They have selected a good graph to represent their data collected.
```

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
```
Unfortunately, the authors do not provide an analysis of the data they have collected.
```

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
```
Raw data -> Results 				: YES, provided
Existing experiment setup -> Data 	: NO, not provided
Set up experiment 					: YES, provided

The authors failed to provide their “RSpec” file. This made it difficult to verify their setup.
They did not provide a provision to login to their existing setup (ie; no guest credentials, no access to their slice)
YES, their instructions were minimalistic and easy to understand to a certain extent. They could have improved their
instructions if they would have provided the necessary commands in the “Report” file rather than mentioning it a separate “readme” file.
```

2) Were you able to successfully produce experiment results?
```
Modprobe tcp_dctcp was not functioning even after installing the files provided.
```

3) How long did it take you to run this experiment, from start to finish?
```
This experiment took me 1 day.
```

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
```
There were a few mistakes made in the commands provided by the authors. I corrected these errors and continued with the experiment

(1)
ERROR: (changing TCP variant)
sysctl -w ipv4.net.tcp__congestion_control=cubic
sysctl net.ipv4.tcp__congestion_control

CORRECTION:
sysctl -w net.ipv4.tcp_congestion_control=VARIANT
sysctl net.ipv4.tcp_congestion_control

(2)
ERROR: The authors did not explicitly mention if we are required to run iperf at both the clients or just one of them while collecting delay information.

ASSUMPTION: I ran iperf at both clients
```

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
```
The degree of reproducibility is “3”
The results can be reproduced by an independent researcher, requiring considerable effort.
```


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
```
Overall this was an excellent attempt at the High Speed Network project.
A few pointers that may help the authors in improving their experiment are:
1. Please mention the procedure and commands necessary in a single file "Report.md". Please do not mentioning it a separate “readme” file.
2. Please provide alternative methods to reproduce the results. Eg: Rspec file and Access to existing slice.
3. Please make the goal less ambiguous and more detailed.
```
